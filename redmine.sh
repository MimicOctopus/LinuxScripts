#! /bin/sh

# REDMINE
# Author: Faeriol
# App Version: 2.3

### BEGIN INIT INFO
# Provides:          redmine
# Required-Start:    $local_fs $remote_fs $network $syslog
# Required-Stop:     $local_fs $remote_fs $network $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Redmine issue management
# Description:       Redmine issue management
### END INIT INFO

### Originally based on the Gitlab init script.

### Environment variables
RAILS_ENV="production"

# Script variable names should be lower-case not to conflict with internal
# /bin/sh variables such as PATH, EDITOR or SHELL.
app_root="/home/redmine/redmine" #Adapt in fuction of your actual install
app_user="redmine"
port="8081" #Adapt in function of your actual install
pid_path="$app_root/tmp/pids"
socket_path="$app_root/tmp/sockets"
passenger_pid_path="$pid_path/passenger.$port.pid"


### Here ends user configuration ###


# Switch to the app_user if it is not he/she who is running the script.
if [ "$(whoami)" != "$app_user" ]; then
  sudo -u "$app_user" -H -i $0 "$@"; exit;
fi

# Switch to the redmine path, if it fails exit with an error.
if ! cd "$app_root" ; then
 echo "Failed to cd into $app_root, exiting!";  exit 1
fi

### Init Script functions

check_pids(){
  if ! mkdir -p "$pid_path"; then
    echo "Could not create the path $pid_path needed to store the pids."
    exit 1
  fi
  # If there exists a file which should hold the value of the Passenger pid: read it.
  if [ -f "$passenger_pid_path" ]; then
    wpid=$(cat "$passenger_pid_path")
  else
    wpid=0
  fi
}

# We use the pids in so many parts of the script it makes sense to always check them.
# Only after start() is run should the pids change.
check_pids


# Checks whether the different parts of the service are already running or not.
check_status(){
  check_pids
  # If passenger is running kill -0 $wpid returns true, or rather 0.
  # Checks of passenger_status should only check for == 0 or != 0, never anything else.
  if [ $wpid -ne 0 ]; then
    kill -0 "$wpid" 2>/dev/null
    passenger_status="$?"
  else
    passenger_status="-1"
  fi
}

# Check for stale pids and remove them if necessary
check_stale_pids(){
  check_status
  # If there is a pid it is something else than 0, the service is running if
  # passenger_status is == 0.
  if [ "$wpid" != "0" -a "$passenger_status" != "0" ]; then
    echo "Removing stale Passenger pid. This is most likely caused by Passenger crashing the last time it ran."
    if ! rm "$passenger_pid_path"; then
      echo "Unable to remove stale pid, exiting"
      exit 1
    fi
  fi
}

# If no parts of the service is running, bail out.
exit_if_not_running(){
  check_stale_pids
  if [ "$passenger_status" != "0" ]; then
    echo "Redmine is not running."
    exit
  fi
}

# Starts Redmine.
start() {
  check_stale_pids

  # Then check if the service is running. If it is: don't start again.
  if [ "$passenger_status" = "0" ]; then
    echo "The Redmine Passenger instance is already running with pid $wpid, not restarting."
  else
    echo "Starting the Redmine Passenger instance..."
    # Remove old socket if it exists
    rm -f "$socket_path"/passenger.socket 2>/dev/null
    
    passenger start -p "$port" -d -e "$RAILS_ENV"
  fi

  # Finally check the status to tell wether or not Passenger is running
  status
}

# Asks Passenger if it would be so kind as to stop, if not kills it.
stop() {
  exit_if_not_running
  # If the Redmine Passenger instance is running, tell it to stop;
  if [ "$passenger_status" = "0" ]; then
    passenger stop -p "$port"
    echo "Stopping the Redmine Passenger instance..."
    stopping=true
  else
    echo "The Redmine Passenger web was not running, doing nothing."
  fi

  # If something needs to be stopped, lets wait for it to stop. Never use SIGKILL in a script.
  while [ "$stopping" = "true" ]; do
    sleep 1
    check_status
    if [ "$passenger_status" = "0"  ]; then
      printf "."
    else
      printf "\n"
      break
    fi
  done
  sleep 1
  # Cleaning up unused pids
  rm "$passenger_pid_path" 2>/dev/null
  status
}

# Returns the status of Redmine and it's components
status() {
  check_status
  if [ "$passenger_status" != "0" ]; then
    echo "Redmine is not running."
    return
  fi
  if [ "$passenger_status" = "0" ]; then
      echo "The Redmine Passenger instance with pid $wpid is running."
  else
      printf "The Redmine Passenger instance is \033[31mnot running\033[0m.\n"
  fi
}

reload(){
  exit_if_not_running
  if [ "$wpid" = "0" ];then
    echo "The Redmine Passenger instance is not running thus its configuration can't be reloaded."
    exit 1
  fi
  printf "Reloading Redmine Passenger configuration... "
  kill -USR2 "$wpid"
  echo "Done."
  # Waiting 2 seconds to let the service settle.
  sleep 2
  status
}

restart(){
  check_status
  if [ "$passenger_status" = "0" ]; then
    stop
  fi
  start
}

## Finally the input handling.

case "$1" in
  start)
        start
        ;;
  stop)
        stop
        ;;
  restart)
        restart
        ;;
  reload|force-reload)
	reload
        ;;
  status)
        status
        ;;
  *)
        echo "Usage: service redmine {start|stop|restart|reload|status}"
        exit 1
        ;;
esac

exit